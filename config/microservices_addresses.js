let addresses = {
  "audit": "http://" + ENV.MICROSERVICE_HOST + ":8095",
  "coverage": "http://" + ENV.MICROSERVICE_HOST + ":8089",
  "project": "http://" + ENV.MICROSERVICE_HOST + ":8050",
  "simcard": "http://" + ENV.MICROSERVICE_HOST + ":8090",
  "irms": "http://" + ENV.MICROSERVICE_HOST + ":8087",
  "document": "http://" + ENV.MICROSERVICE_HOST + ":8086",
  "tadig": "http://" + ENV.MICROSERVICE_HOST + ":5000",
  "gser": "http://" + ENV.MICROSERVICE_HOST + ":8091",
  "doc-creator": "http://" + ENV.MICROSERVICE_HOST + ":8092",
  "deal": "http://" + ENV.MICROSERVICE_HOST + ":8093",
  "pm": "http://" + ENV.MICROSERVICE_HOST + ":8094",
  "changeRequest": "http://" + ENV.MICROSERVICE_HOST + ":8088",
  "log-tracker": "http://" + ENV.MICROSERVICE_HOST + ":8097",
  "analytics": "http://" + ENV.MICROSERVICE_HOST + ":8098",
  "steerop": "http://" + ENV.MICROSERVICE_HOST + ":8099",
  "baseUrl": "http://" + ENV.MICROSERVICE_HOST + ":4200",


}


module.exports = addresses
