
module.exports = {
  httpAdminRoot: "/red",
  httpNodeRoot: "/api",
  userDir: ".node-red/",
  functionGlobalContext: {}, // enables global context
    editorTheme: {
      theme: "dark"
  },
  // Configure the logging output
logging: {
    // Console logging



    // console: {
    //     level: "info",
    //     metrics: false,
    //     audit: false
    // },


    // Custom logger
    myCustomLogger: {
        level: 'info',
        metrics: false,
        handler: function(settings) {
            // Called when the logger is initialised

            // Return the logging function
            return function(msg) {
                console.log(msg.timestamp, msg.msg);
            }
        }
    }
}

};
