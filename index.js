require('dotenv').config()
require('./config/envirement')
const http = require('http')
const path = require('path')

const express = require('express')
const RED = require('node-red')

const app = express()
const server = http.createServer(app)
const mongoose = require('mongoose')

const moment = require("moment")

const settings = {
  httpAdminRoot: '/',
  httpNodeRoot: '/', // /api
  userDir: './',
  flowFile: 'flows.json',
  apiMaxLength: '50mb',
  editorTheme: {
    page: {
      css: path.join(__dirname, "/ui-styles/midnight.css"),
      scripts: path.join(__dirname, "/ui-styles/theme-tomorrow.js")
    },
    palette: {
        editable: false
    }
  },
  functionGlobalContext: { // enables global context

    "testValue": "exampleString",

    "testPackage": moment

  }
}

function startNR(settings) {
  console.log(`[INFO] Global settings for Node-Red: ${JSON.stringify(settings.functionGlobalContext)}`)
  const port = 9999

  RED.init(server, settings)

  app.use(settings.httpAdminRoot, RED.httpAdmin)

  app.use(settings.httpNodeRoot, RED.httpNode)

  server.listen(port)

  RED.start()
}

startNR(settings)


//Connect to mongoDB:

mongoose.Promise = global.Promise
mongoose.connect(
  ENV.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)
global.Models = require("./models/")
app.get('/getModels',function(req,res,next){
  let models=Object.keys(require("./models/"));
  res.send(models)
})
app.get('/getModelFields/:model',function(req,res,next){
  let paths=require("./models/")[req.params.model].schema.paths;
let fields=[];

    for (const [key, value] of Object.entries(paths)) {
let populateOne=(!!value.options.ref);
let populateMany= (!!((value.options.type[0])&&(value.options.type[0].ref)));
if(populateOne || populateMany) fields.push(key)
  }

res.send(fields);
})
