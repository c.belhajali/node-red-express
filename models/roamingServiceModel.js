const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "RoamingService"
};

roamingServiceSchema = new Schema({
  name: {
    type: String,
  },
  rank: {
    type: Number,
  },

  Customer: {
    type: Schema.Types.ObjectId,
    ref: 'Customer'
  },

}, {
  timestamps: true
})

const RoamingService = mongoose.model('RoamingService', roamingServiceSchema)
module.exports = {
  RoamingService,
  roamingServiceSchema,
  collection
}
