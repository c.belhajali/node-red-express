const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
    collectionName: "Zone"
};

const zoneSchema = new Schema({
    name: {
        type: String
    },
    operators: [{
        type: Schema.Types.ObjectId,
        ref: 'Operator'
    }],
    networks: [{
        type: Schema.Types.ObjectId,
        ref: 'Network'
    }]
}, {
    timestamps: true
})

const Zone = mongoose.model('Zone', zoneSchema)
module.exports = {
    Zone,
    zoneSchema,
    collection
}
