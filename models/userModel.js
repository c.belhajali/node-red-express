const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const collection = {
  collectionName: "User"
};

const userNotificationSchema = require('./userNotificationModel').userNotificationSchema;

const userSchema = new Schema({
  user_name: {
    type: String,
    required: true,
    unique: true
  },
  passwordHash: {
    type: String,
  },

  first_name: {
    type: String,
  },
  last_name: {
    type: String,
  },
  email: {
    type: String,
    required: true
  },
  team: {
    type: String,
  },
  is_delegator: {
    type: Boolean,
    required: true,
    default: false
  },
  user_system: {
    type: Boolean,
    required: true,
    default: false
  },
  user_roamsmart: {
    type: Boolean,
    required: true,
    default: false
  },
  disabled: {
    type: Boolean,
    required: true,
    default: false
  },
  deleted: {
    type: Boolean,
    required: true,
    default: false
  },
  roaming_coordinator: {
    type: Boolean,
    required: true,
    default: false
  },
  agreement_owner: {
    type: Boolean,
    required: true,
    default: false
  },
  project_owner: {
    type: Boolean,
    required: true,
    default: true
  },
  account_manager: {
    type: Boolean,
    required: true,
    default: true
  },
  non_completed_project_report: {
    type: Boolean,
    required: true,
    default: false
  },
  weekly_launch_report: {
    type: Boolean,
    required: true,
    default: false
  },
  monthly_launch_report: {
    type: Boolean,
    required: true,
    default: false
  },
  monthly_full_report: {
    type: Boolean,
    required: true,
    default: false
  },
  launched_report: {
    type: String,
  },

  Customer: {
    type: Schema.Types.ObjectId,
    ref: 'Customer'
  },
  networks: [{
    type: Schema.Types.ObjectId,
    ref: 'Network'
  }],



  userNotifications: [userNotificationSchema],
}, {
  timestamps: true
});

const User = mongoose.model("User", userSchema);
module.exports = {
  User,
  userSchema,
  collection
}
