const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "MailHistory"
};

const mailHistorySchema = new Schema({

  subject: {
    type: String,
  },
  mail_from: {
    type: String,
  },
  mail_to: {
    type: String,
  },
  body: {
    type: String,
  },
  send_date: {
    type: Date,
  },
  Project: {
    type: Schema.Types.ObjectId,
    ref: 'Project'
  },

}, {
  timestamps: true
})
const MailHistory = mongoose.model('MailHistory', mailHistorySchema)
module.exports = {
  MailHistory,
  mailHistorySchema,
  collection
}
