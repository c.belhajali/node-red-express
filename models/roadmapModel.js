const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "Roadmap"
};

const roadmapSchema = new Schema({

  name: {
    type: String,
  },


  description: {
    type: String,
  },


  start_date: {
    type: Date,
  },


  end_date: {
    type: Date,
  },

  // projects: [{
  //   type: Schema.Types.ObjectId,
  //   ref: 'Project'
  // }],

  networks: [{
    type: Schema.Types.ObjectId,
    ref: 'Network'
  }],
}, {
  timestamps: true
})

const Roadmap = mongoose.model('Roadmap', roadmapSchema)
module.exports = {
  Roadmap,
  roadmapSchema,
  collection
}
