const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "Country"
};

operatorSchema = require('./operatorModel').operatorSchema;

const countrySchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  alpha2: {
    type: String,
    required: false,
    index: true
  },
  alpha3: {
    type: String,
    required: false,
  },
  cc: {
    type: String,
    //  required: true,
  },

}, {
  timestamps: true
})

const Country = mongoose.model('Country', countrySchema)
module.exports = {
  Country,
  countrySchema,
  collection
}
