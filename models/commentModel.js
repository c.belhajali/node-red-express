const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "Comment"
};

const commentSchema = new Schema({
  comment: {
    type: String,
  },

  comment_date: {
    type: Date,
  },
  created_by: {
    type: String,
  },
  User:{
      type: Schema.Types.ObjectId,
      ref: 'User'
  }
}, {
  timestamps: true
})

const Comment = mongoose.model('Comment', commentSchema)
module.exports = {
  Comment,
  commentSchema,
  collection
}
