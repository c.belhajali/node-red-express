const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "CoverageRoamingService"
};

const coverageRoamingServiceSchema = new Schema({
  direction: {
    type: String,
  },
  status: {
    type: String,
  },
  service: {
    type: String,
  },
  launchDate: {
    type: Date,
  },
  sunsetDate: {
    type: Date,
  },
  lastUpdate: {
    type: Date,
  },

  RoamingService: {
    type: Schema.Types.ObjectId,
    ref: 'RoamingService'
  },

}, {
  timestamps: true
})
const CoverageRoamingService = mongoose.model('CoverageRoamingService', coverageRoamingServiceSchema)
module.exports = {
  CoverageRoamingService,
  coverageRoamingServiceSchema,
  collection
}
