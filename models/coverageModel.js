const mongoose = require('mongoose')
const Schema = mongoose.Schema;
let commentSchema = require('./commentModel').commentSchema;
let coverageRoamingServiceSchema = require('./coverageRoamingServiceModel').coverageRoamingServiceSchema;
const collection = {
  collectionName: "Coverage"
};
const {
  tagSchema
} = require("./tagModel");

const coverageSchema = new Schema({
  tadigNetwork: {
    type: String,
  },
  tadigOperator: {
    type: String,
  },
  alpha2: {
    type: String,
    index: true
  },

  country_name: {
    type: String,
  },

  mccmnc: {
    type: String,
  },

  operator_name: {
    type: String,
  },
  internal_id: {
    type: String,
  },
  agreement_status: {
    type: String,
    required: false,
  },
  agreement_version: {
    type: String,
  },
  agreement_type: {
    type: String,
  },
  agreement_date: {
    type: Date,
  },

  agreement_owner: {
    type: String,
  },
  account_manager: {
    type: String

  },
  ir21_distribution_email: [{
    type: String,
  }],

  sms_interworking: {
    type: String,
  },
  alias: {
    type: String,
    required: true
  },
  status: {
    type: String,
    index: true,
    enum: ['Live', 'Not Live', 'In progress', 'Terminated', 'Blocked Inbound', 'Blocked Outbound', 'Blocked Bilateral', null, "NOT Live"]
  },
  relation: {
    type: String,
  },
  region: {
    type: String,
  },
  operator_group: {
    type: String,
  },
  zone: {
    type: String,
  },
  is_terminated: {
    type: Boolean,
    required: true,
    default: false
  },
  termination_date: {
    type: Date,
  },
  color_code: {
    type: String,
  },
  active: {
    type: Boolean,
    required: true,
    default: true
  },
  is_principal: {
    type: Boolean,
    default: true
  },
  ipx_routing: {
    type: String,
  },
  sms_price: {
    type: Number,
  },
  is_mnp: {
    type: Boolean,
    required: true,
    default: false
  },
  ColorCode: {
    type: Schema.Types.ObjectId,
    ref: 'ColorCode'
  },
  Operator: {
    type: Schema.Types.ObjectId,
    ref: 'Operator'
  },
  tags: [{
    type: Schema.Types.ObjectId,
    ref: 'Tag'
  }],
  Zone: {
    type: Schema.Types.ObjectId,
    ref: 'Zone'
  },
  Group: {
    type: Schema.Types.ObjectId,
    ref: 'Group'
  },
  Region: {
    type: Schema.Types.ObjectId,
    ref: 'Region'
  },
  Network: {
    type: Schema.Types.ObjectId,
    ref: 'Network'
  },
  comments: [commentSchema],
  coverageRoamingServices: [{
    type: Schema.Types.ObjectId,
    ref: 'CoverageRoamingService'
  }],
}, {
  timestamps: true
})
coverageSchema.index({
  tadigNetwork: 1,
  alias: 1,
  alpha2: 1,
  tadigOperator: 1
}, {
  unique: true
});

const Coverage = mongoose.model('Coverage', coverageSchema)
module.exports = {
  Coverage,
  coverageSchema,
  collection
}
