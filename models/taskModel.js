const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "Task"
};
const commentSchema = require('./commentModel').commentSchema;

const taskSchema = new Schema({
  name: {
    type: String,
  },
  status: {
    type: String,
    enum: ["Completed", "Blocked", "Escalated", "In progress", "Open", "Troubleshooting"],
    default: "Open"
  },
  track: {
    type: String,
    required: true,
    default: "ON_TRACK"
  },

  reminder_date: {
    type: Date,
  },
  elapsed_time: {
    type: Number,
  },
  default_status: {
    type: String,
  },
  time_estimate: {
    type: Number,
  },
  in_progress_date: {
    type: Date,
  },
  itsm_sent: {
    type: Boolean,
    required: true,
    default: false
  },
  to_notify: {
    type: Boolean,
  },
  notified: {
    type: Boolean,
    required: true,
    default: false
  },
  status_date: {
    type: Date,
  },
  system_task_ID: {
    type: String,
  },
  system_ticket_ID: {
    type: String,
  },
  itsm_ack: {
    type: Boolean,
    required: true,
    default: false
  },

  task_category: {
    type: String,
  },
  launch_task: {
    type: Boolean,
    required: true,
    default: false
  },
  tadig_task: {
    type: Boolean,
    required: true,
    default: false
  },
  ireg_task: {
    type: Boolean,
    required: true,
    default: false
  },
  termination_task: {
    type: Boolean,
    required: true,
    default: false
  },
  completed_task: {
    type: Boolean,
    required: true,
    default: false
  },
  external_notification: {
    type: Boolean,
    required: true,
    default: false
  },
  network: {
    type: String,
  },
  user_assigned: {
    type: String,
  },
  termination_date: {
    type: Date
  },
  User: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  documents: [{
    type: String,
    required: true,
  }],

  user: {
    type: String,
  },

  comments: [commentSchema],


}, {
  timestamps: true,
  autoCreate: false,

})

taskSchema.virtual('documentsPdf', {
  ref: 'Document', // The model to use
  localField: 'documents', // Find people where `localField`
  foreignField: 'pdfFileId' // is equal to `foreignField`
});
taskSchema.set('toObject', {
  virtuals: true
});

// taskSchema.pre("save", function(next) {
//   //syncronize the status with the default_status
//   let self = this
//   console.log(this)
//   if (this.isNew){
//     console.log(this)
//   self.status = self.default_status
// }
//
//   next();
// })

const Task = mongoose.model('Task', taskSchema)
module.exports = {
  Task,
  taskSchema,
  collection
}
