const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "TaskHistory"
};

const taskHistorySchema = new Schema({

  status: {
    type: String,
  },
  milestone: {
    type: String,
  },
  task: {
    type: String,
  },
  
  status_date: {
    type: Date,
  },
  changed_by: {
    type: String,
  },
  previous_status: {
    type: String,
  },
  Project: {
    type: Schema.Types.ObjectId,
    ref: 'Project'
  },

}, {
  timestamps: true
})

const TaskHistory = mongoose.model('TaskHistory', taskHistorySchema)
module.exports = {
  TaskHistory,
  taskHistorySchema,
  collection
}
