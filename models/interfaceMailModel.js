const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "InterfaceMail"
};

const interfaceMailSchema = new Schema({
  sender: {
    type: Boolean,
  },
  email: {
    type: String,
  },
  external_notification: {
    type: Boolean,
    required: true,
    default: false
  },
  launch_report_notification: {
    type: Boolean,
    required: true,
    default: false
  },
  description: {
    type: String,
  },
  tadig_code:{
    type: String,
  }
}, {
  timestamps: true
})

const InterfaceMail = mongoose.model('InterfaceMail', interfaceMailSchema)
module.exports = {
  InterfaceMail,
  interfaceMailSchema,
  collection
}
