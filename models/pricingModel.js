const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
    collectionName: "Pricing"
};

const pricingSchema = new Schema({
  network: {
    type: String,
  },
  alias: {
    type: String,
  },
    tags: [{
        type: Schema.Types.ObjectId,
        ref: 'Tag'
    }]
}, {
    timestamps: true
})

const Pricing = mongoose.model('Pricing', pricingSchema)
module.exports = {
    Pricing,
    pricingSchema,
    collection
}
