const mongoose = require('mongoose')
const Schema = mongoose.Schema;
// const countrySchema= require('./countryModel').countrySchema;
const collection = {
  collectionName: "Operator"
};

const operatorSchema = new Schema({
  tadig: {
    type: String,
    required: true,
    index: true,
  },
  alias: {
    type: String,
  },
  name: {
    type: String,
    required: true,
  },

  mccmnc: {
    type: String
  },

  // coverages: [{
  //   type: Schema.Types.ObjectId,
  //   ref: 'Coverage'
  // }],
  is_operator: {
    type: Boolean,
    default: true
  },
  is_principal: {
    type: Boolean,
    default: true
  },
  Country: {
    type: Schema.Types.ObjectId,
    ref: 'Country'
  },
  // Network: {
  //   type: Schema.Types.ObjectId,
  //   ref: 'Network'
  // },
}, {
  timestamps: true
})

const Operator = mongoose.model('Operator', operatorSchema)
module.exports = {
  Operator,
  operatorSchema,
  collection
}
