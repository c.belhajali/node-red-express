const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "ProjectType"
};

const projectTypeSchema = new Schema({

  label: {
    type: String,
  },

  Network: {
    type: Schema.Types.ObjectId,
    ref: 'Network'
  },

}, {
  timestamps: true
})

const ProjectType = mongoose.model('ProjectType', projectTypeSchema)
module.exports = {
  ProjectType,
  projectTypeSchema,
  collection
}
