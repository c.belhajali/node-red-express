const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "UserNotification"
};

const userNotificationSchema = new Schema({
  new_task: {
    type: Boolean,
    required: true,
    default: true
  },
  milestone_completed: {
    type: Boolean,
    required: true,
    default: true
  },
  project_completed: {
    type: Boolean,
    required: true,
    default: false
  },
  overdue_task: {
    type: Boolean,
    required: true,
    default: false
  },
  new_launches: {
    type: Boolean,
    required: true,
    default: false
  },
  in_progress_project: {
    type: Boolean,
    required: true,
    default: false
  },
  new_project: {
    type: Boolean,
    required: true,
    default: false
  },
  project_completed_monthly: {
    type: Boolean,
    required: true,
    default: false
  },
  project_completed_weekly: {
    type: Boolean,
    required: true,
    default: false
  },
  inProgress_weekly: {
    type: Boolean,
    required: true,
    default: false
  },

  // User: {
  //   type: Schema.Types.ObjectId,
  //   ref: 'User'
  // },

  Network: {
    type: Schema.Types.ObjectId,
    ref: 'Network'
  },

}, {
  timestamps: true
})

const UserNotification = mongoose.model('UserNotification', userNotificationSchema)
module.exports = {
  UserNotification,
  userNotificationSchema,
  collection
}
