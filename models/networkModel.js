const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const mailTemplateSchema = require('./mailTemplateModel').mailTemplateSchema;
const projectTargetSchema = require('./projectTargetModel').projectTargetSchema;
const operatorSchema = require('./operatorModel').operatorSchema;
const interfaceMailSchema = require('./interfaceMailModel').interfaceMailSchema;
const distributionMailSchema = require('./distributionMailModel').distributionMailSchema;
const projectTemplateSchema = require('./projectTemplateModel').projectTemplateSchema;
const collection = {
  collectionName: "Network"
};

const networkSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  tadig: {
    type: String,
    required: true,
  },
  alias: {
    type: String
  },
  Customer: {
    type: Schema.Types.ObjectId,
    ref: 'Customer'
  },
  Operator: {
    type: Schema.Types.ObjectId,
    ref: 'Operator'
  },

  is_principal: {
    type: Boolean,
    default: true
  },
  interfaceMails: [interfaceMailSchema],

  mailTemplates: [mailTemplateSchema],

  distributionMails: [distributionMailSchema],



}, {
  timestamps: true
})

const Network = mongoose.model('Network', networkSchema)
module.exports = {
  Network,
  networkSchema,
  collection
}
