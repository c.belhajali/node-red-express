const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const colorCodeSchema = require('./colorCodeModel').colorCodeSchema;
const roamingServiceSchema = require('./roamingServiceModel').roamingServiceSchema;
const collection = {
  collectionName: "Customer"
};

const customerSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  network_multiple: {
    type: Boolean,
    required: true,
    default: false
  },
  daily_report: {
    type: Boolean,
    required: true,
    default: false
  },
  history_report: {
    type: Boolean,
    required: true,
    default: false
  },
  ssh_server: {
    type: String,
  },
  ssh_username: {
    type: String,
  },
  ssh_password: {
    type: String,
  },
  ssh_private_key: {
    type: String,
  },




}, {
  timestamps: true
})

const Customer = mongoose.model('Customer', customerSchema)
module.exports = {
  Customer,
  customerSchema,
  collection
}
