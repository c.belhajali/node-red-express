const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "ProjectTemplate"
};
milestoneSchema = require('./milestoneModel').milestoneSchema;

const projectTemplateSchema = new Schema({

  name: {
    type: String,
  },
  direction: {
    type: String,
    set: function(direction) {
      this._direction = this.direction;
      return direction;
    }
  },
  relation: {
    type: String,
    set: function(relation) {
      this._relation = this.relation;
      return relation;
    }
  },
  termination_template: {
    type: Boolean,
    required: true,
    default: false
  },
  fast_track: {
    type: Boolean,
    required: true,
    default: false
  },
  lte_only: {
    type: Boolean,
    required: true,
    default: false
  },
  remotely: {
    type: Boolean,
    required: true,
    default: false
  },

  tadigNetwork: [{
    type: String
  }],
  networks: [{
    type: Schema.Types.ObjectId,
    ref: 'Network',
    set: function(networks) {
      this._networks = this.networks;
      return networks;
    }
  }],
  milestones: [milestoneSchema],
  roamingServices: [{
    type: Schema.Types.ObjectId,
    ref: 'RoamingService',
    set: function(Roaming_services) {
      this._Roaming_services = this.Roaming_services;
      return Roaming_services;
    }
  }],
}, {
  timestamps: true
})
projectTemplateSchema.index({
  networks: 1,
  roamingServices: 1,
  direction: 1,
  fast_track: 1,
  remotely: 1,
  termination_template: 1,
  lte_only: 1,
  relation: 1
}, {
  unique: true
});


projectTemplateSchema.pre("save", function(next) {
  let self = this

  if ((self.createdAt == self.updatedAt) || self.isModified('networks') ||
    self.isModified('roamingServices') || self.isModified('direction') ||
    self.isModified('relation')) {
    Models.ProjectTemplate.findOne({
      networks: self.networks,
      roamingServices: self.roamingServices,
      direction: self.direction,
      termination_template: self.termination_template,
      relation: self.relation,
      tadigNetwork: self.tadigNetwork,
      fast_track:self.fast_track,
      remotely:self.remotely,
      lte_only:self.lte_only
    }, function(err, projectTemplate) {

      if (projectTemplate) {
        self.invalidate("direction", "(roamingServices, networks, direction,termination_template, relation, remotely, lte_only) combination must be unique");
        next(new Error('ProjectTemplate Combination exists'));
      } else {
        next();
      }
    })
  } else {
    next();
  }

});


const ProjectTemplate = mongoose.model('ProjectTemplate', projectTemplateSchema)
module.exports = {
  ProjectTemplate,
  projectTemplateSchema,
  collection
}
