const mongoose = require('mongoose')
const {commentSchema} = require("./commentModel");
const Schema = mongoose.Schema;
const collection = {
  collectionName: "Tag"
};

const tagSchema = new Schema({
  name: {
    type: String,
  },
  color: {
    type: String,
  },
  views: [{
    type: String,
  }],
  created_by: {
    type: String,
  },
  updated_by: {
    type: String,
  },
  networks: [{
    type: String,
  }],
  tadigs: [{
    type: String,
  }],
  Network: [{
    type: Schema.Types.ObjectId,
    ref: 'Network',asindex:true
  }],
  Operator: [{
    type: Schema.Types.ObjectId,
    ref: 'Operator',asindex:true
  }],
  comments: [commentSchema]
}, {
  timestamps: true
})

const Tag = mongoose.model('Tag', tagSchema)
module.exports = {
  Tag,
  tagSchema,
  collection
}
