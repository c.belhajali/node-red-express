const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
    collectionName: "Region"
};

const tagSchema = new Schema({
    name: {
        type: String,
    },
    operators: [{
        type: Schema.Types.ObjectId,
        ref: 'Operator'
    }],
    networks: [{
        type: Schema.Types.ObjectId,
        ref: 'Network'
    }]
}, {
    timestamps: true
})

const Region = mongoose.model('Region', tagSchema)
module.exports = {
    Region,
    tagSchema,
    collection
}
