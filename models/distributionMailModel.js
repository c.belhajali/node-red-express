const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "DistributionMail"
};

const distributionMailSchema = new Schema({

  name: {
    type: String,
  },
  emails: {
    type: String,
  },
  tadig_code:{
    type: String,
  }
}, {
  timestamps: true
})

const DistributionMail = mongoose.model('DistributionMail', distributionMailSchema)
module.exports = {
  DistributionMail,
  distributionMailSchema,
  collection
}
