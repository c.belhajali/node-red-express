const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "MailTemplate"
};

const mailTemplateSchema = new Schema({

  title: {
    type: String,
  },
  body: {
    type: String,
  },
  tadig_code:{
    type: String,
  }

}, {
  timestamps: true
})

const MailTemplate = mongoose.model('MailTemplate', mailTemplateSchema)
module.exports = {
  MailTemplate,
  mailTemplateSchema,
  collection
}
