const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
    collectionName: "Group"
};

const groupSchema = new Schema({
    name: {
        type: String,
    },
    operators: [{
        type: Schema.Types.ObjectId,
        ref: 'Operator'
    }],
    networks: [{
        type: Schema.Types.ObjectId,
        ref: 'Network'
    }]
}, {
    timestamps: true
})

const Group = mongoose.model('Group', groupSchema)
module.exports = {
    Group,
    groupSchema,
    collection
}
