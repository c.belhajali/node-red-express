const mongoose = require('mongoose')

const Schema = mongoose.Schema;
const collection = {
  collectionName: "ColorCode"
};
const colorCodeSchema = new Schema({
  status: {
    type: String,
  },
  code: {
    type: String,
  },
  rank: {
    type: Number,
  },

  Customer: {
    type: Schema.Types.ObjectId,
    ref: 'Customer'
  },
}, {
  timestamps: true
})
const ColorCode = mongoose.model('ColorCode', colorCodeSchema)
module.exports = {
  ColorCode,
  colorCodeSchema,
  collection
}
