const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "document"
};


const documentSchema = new Schema({

  network: {
    type: String,
  },
  tadig: {
    type: String,
  },
  alias: {
    type: String,
  },
  xmlFileName: {
    type: String,
  },
  pdfFileName: {
    type: String,
  },
  xmlFileId: {
    type: String,
  },
  pdfFileId: {
    type: String,
  },

  source: {
    type: String,
  },
  docType: {
    type: String,
  },
  status: {
    type: String,
  },
  creationDate: {
    type: Date,
  },

  is_project: {
    type: Boolean,
    default: false,
  },

}, {
  timestamps: true
})

const Document = mongoose.model('Document', documentSchema)
module.exports = {
  Document,
  documentSchema,
  collection
}
