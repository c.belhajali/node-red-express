const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const colorCodeSchema = require('./colorCodeModel').colorCodeSchema,
  commentSchema = require('./commentModel').commentSchema,
  milestoneSchema = require('./milestoneModel').milestoneSchema;

const collection = {
  collectionName: "Project"
};
const {
  tagSchema
} = require("./tagModel");

const projectSchema = new Schema({
  tadigNetwork: [{
    type: String,
    index: true
  }],
  alias: {
    type: String,
    index: true
  },
  tadigCode: {
    type: String,
    index: true
  },
  direction: {
    type: String,
    index: true
  },
  alpha2: {
    type: String
  },
  country_name: {
    type: String,
    index: true
  },
  mccmnc: {
    type: String,
    index: true
  },
  operator_name: {
    type: String,
    index: true
  },
  relation: {
    type: String,
    index: true
  },
  operator_group: {
    type: String,
  },
  zone: {
    type: String,
  },
  region: {
    type: String,
  },
  priority: {
    type: String,
    index: true
  },
  status: {
    type: String,
    enum: ["Completed", "Blocked", "Escalated", "In progress", "Open", "Planned", "Terminated", "On hold"],
    default: "Open",
    index: true
  },
  status_date: {
    type: Date,
  },
  start_date: {
    type: Date,
  },
  due_date: {
    type: Date,
  },

  termination_date: {
    type: Date,
  },
  is_terminated: {
    type: Boolean,
    required: true,
    default: false
  },
  on_hold: {
    type: Boolean,
    required: true,
    default: false
  },
  old_status: {
    type: String,
  },
  current_milestone: {
    type: String,
    index: true
  },
  last_milestone: {
    type: String,
  },
  last_milestone_date: {
    type: Date,
  },
  last_updated_milestone: {
    type: String,
  },
  last_milestone_progress: {
    type: Number,
  },
  last_task: {
    type: String,
  },
  last_task_status: {
    type: String,
  },
  last_task_update: {
    type: String,
  },
  last_task_user: {
    type: String,
  },
  services_list: [{
    type: String,
    index: true
  }],
  creation_date: {
    type: Date,
  },
  launch_date: {
    type: Date,
    index: true
  },
  is_separated: {
    type: Boolean,
    required: true,
    default: true
  },

  is_reopened: {
    type: Boolean,
    required: true,
    default: false
  },
  fast_track: {
    type: Boolean,
    required: true,
    default: false
  },
  remotely: {
    type: Boolean,
    required: true,
    default: false
  },
  lte_only: {
    type: Boolean,
    required: true,
    default: false
  },
  service_ready: {
    type: String,
  },
  termination_project: {
    type: Boolean,
    required: true,
    default: false
  },
  last_comment: {
    type: String,
  },
  process_name: {
    type: String,
  },
  is_archived: {
    type: Boolean,
    required: true,
    default: false
  },
  roaming_coordinator: {
    type: String,
  },
  project_owner: {
    type: String

  },
  Customer: {
    type: Schema.Types.ObjectId,
    ref: 'Customer'
  },
  Roadmap: {
    type: Schema.Types.ObjectId,
    ref: 'Roadmap'
  },
  tags: [{
        type: Schema.Types.ObjectId,
        ref: 'Tag'
    }],

    Zone: {
        type: Schema.Types.ObjectId,
        ref: 'Zone'
    },
    Group: {
        type: Schema.Types.ObjectId,
        ref: 'Group'
    },
    Region: {
        type: Schema.Types.ObjectId,
        ref: 'Region'
    },
  ProjectType: {
    type: Schema.Types.ObjectId,
    ref: 'ProjectType'
  },

  ProjectTemplate: {
    type: Schema.Types.ObjectId,
    ref: 'ProjectTemplate'
  },

  ColorCode: {
    type: Schema.Types.ObjectId,
    ref: 'ColorCode'
  },
  Owner: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  created_by: {
    type: String,
  },

  comments: [commentSchema],
  milestones: [milestoneSchema],
  documents: [{
    type: String,
  required: true,
  }],
  taskDocuments: [{
    type: String,
  required: true,
  }],

  coverages: [{
    type: Schema.Types.ObjectId,
    ref: 'Coverage'
  }],
  roamingServices: [{
    type: Schema.Types.ObjectId,
    ref: 'RoamingService'
  }]
}, {
  timestamps: true
})


projectSchema.virtual('documentsPdf', {
  ref: 'Document', // The model to use
  localField: 'documents', // Find people where `localField`
  foreignField: 'pdfFileId' // is equal to `foreignField`
});
projectSchema.set('toObject', { virtuals: true });
projectSchema.set('toJSON', { virtuals: true });


projectSchema.pre("save", function(next) {
  let self = this

  if ((self.createdAt == self.updatedAt) || self.isModified('tadigNetwork') ||
    self.isModified('alias') || self.isModified('direction') ||
    self.isModified('services_list') || self.isModified('relation') ||
    self.isModified('alpha2') ||
    self.isModified('termination_project') ||
    self.isModified('remotely') ||
    self.isModified('lte_only') ||
    self.isModified('fast_track'))

  {

    Models.Project.findOne({
      _id:{$ne:self._id},
      tadigNetwork: {$in:self.tadigNetwork},
      alias: self.alias,
      direction: self.direction,
      services_list: {$in:self.services_list},
      relation: self.relation,
      alpha2: self.alpha2,
      fast_track: self.fast_track,
      termination_project:self.termination_project,
      remotely:self.remotely,
      lte_only:self.lte_only,
      is_archived:false
    }, function(err, project) {
      if (project) {
        //throw  self.invalidate("alias", "(alias, tadigNetwork, direction, services_list, relation, alpha2, fast_track) combination must be unique");
        self.invalidate("alias", "(alias, tadigNetwork, direction, services_list, relation, alpha2,termination_project, fast_track, remotely, lte_only) combination must be unique");
        next(new Error(`Project Combination exists alias:${project.alias} ,tadigNetwork:${project.tadigNetwork} ,direction:${project.direction} ,services_list:${project.services_list} ,relation:${project.relation} ,alpha2:${project.alpha2} ,termination_project:${project.termination_project} ,fast_track:${project.fast_track} ,remotely:${project.remotely} ,lte_only:${project.lte_only}`));
      } else {
        next();
      }
    })
  } else {
    next();
  }

});

const Project = mongoose.model('Project', projectSchema)
module.exports = {
  Project,
  projectSchema,
  collection
}
