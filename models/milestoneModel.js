const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const taskSchema = require('./taskModel').taskSchema;
const collection = {
  collectionName: "Milestone"
};

const milestoneSchema = new Schema({
  name: {
    type: String,
  },
  rank: {
    type: Number,
  },
  status: {
    type: String,
    enum:["Completed","Blocked","Escalated","In progress","Open"],
    default: "Open"
  },
  progress: {
    type: Number,
  },
  locked: {
    type: Boolean,
    required: true,
    default: false
  },
  agreement_setup: {
    type: Boolean,
    required: true,
    default: false
  },
  agreement_termination: {
    type: Boolean,
    required: true,
    default: false
  },
  launch_milestone: {
    type: Boolean,
    required: true,
    default: false
  },
  termination_milestone: {
    type: Boolean,
    required: true,
    default: false
  },
  notified_milestone: {
    type: Boolean,
    required: true,
    default: false
  },

  active_on: {
    type: Date,
  },

  // ProjectTemplate: {
  //   type: Schema.Types.ObjectId,
  //   ref: 'ProjectTemplate'
  // },

  tasks: [taskSchema],


}, {
  timestamps: true,
  autoCreate: false,

})

const Milestone = mongoose.model('Milestone', milestoneSchema)
module.exports = {
  Milestone,
  milestoneSchema,
  collection
}
