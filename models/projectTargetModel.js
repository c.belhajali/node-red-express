const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const collection = {
  collectionName: "ProjectTarget"
};

const projectTargetSchema = new Schema({

  target: {
    type: Number,
  },
  year: {
    type: String,
    index: true,
  },
  month: {
    type: String,
    index: true
  },


  Network: {
    type: Schema.Types.ObjectId,
    ref: 'Network',
    index: true
  },
  RoamingService: {
    type: Schema.Types.ObjectId,
    ref: 'RoamingService'
  },
}, {
  timestamps: true
})

const ProjectTarget = mongoose.model('ProjectTarget', projectTargetSchema)
module.exports = {
  ProjectTarget,
  projectTargetSchema,
  collection
}
