
const { Machine } = require("xstate");

module.exports = function(RED) {
    function XstateNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {
          const workFlow =
            Machine({
              id: "step",
              initial: "one",
              states: {
                one: {
                  on: { NEXT: "two" },
                },
                two: {
                  on:{ NEXT: "three", PREV: "one" },
                },
                three: {
                  type: "final",
                },
              },
            });
            global.currentState = workFlow.initialState.value;
            global.workFlow = workFlow;

            node.send(msg);
        });
    }
    RED.nodes.registerType("xstate",XstateNode);
}
