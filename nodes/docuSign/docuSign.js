let helper = require('../../_helpers/jwtConsole');
module.exports = function (RED) {
    function DocuSignNode(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        node.on('input', async function (msg) {
            let id = await helper.docuSignHelper(JSON.parse(config.signers));
            node.send([id,null]);
        });
    }

    RED.nodes.registerType("docuSign", DocuSignNode);
}
