module.exports = function(RED) {
  RED.nodes.Models = Models;

  function MongooseFlow(config) {
    RED.nodes.createNode(this, config);
    var node = this;

    node.on('input', async function(msg) {
      try {
        //cast the popolation input value to mongoose format
        let population = config.populate.toString().split(",").join(" ");

        let condition = JSON.parse(config.condition)
        //add values from body to the condition
      //  fillBody(msg, condition)
        if(config.model==="insertMany") condition=msg.req.body;
        msg.result = await Models[config.model_name][config.method](condition).populate(population);
        this.status({
          fill: "green",
          shape: "dot",
          text: "passed"
        });

        return node.send([msg, null]);

      } catch (e) {
        this.status({
          fill: "red",
          shape: "dot",
          text: e
        });
        msg.payload = e
        console.log(msg.result)
        return node.send([null, msg]);
      }


    });
  }
  RED.nodes.registerType("mongoose", MongooseFlow);
}

function fillBody(msg, condition) {
  if(msg.req.body)
  for (const [key, value] of Object.entries(msg.req.body)) {
    if (!condition[key]) condition[key] = value

  }
}
