

module.exports = function(RED) {
    function StateNode(config) {
        RED.nodes.createNode(this,config);
        var node = this;
          node.on('deploy', function(msg) {
          this.status({fill:"blue",shape:"ring",text:global.currentState});
            });
        node.on('input', function(msg) {
          global.currentState=  global.workFlow.transition(global.currentState, "NEXT").value;
            this.status({fill:"blue",shape:"ring",text:global.currentState});
        });
    }
    RED.nodes.registerType("state",StateNode);
}
